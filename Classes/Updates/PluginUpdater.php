<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_facts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Facts\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3factsPluginUpdater')]
class PluginUpdater implements UpgradeWizardInterface
{
    private const SOURCE_CTYPE = 'hivefacts_facts';
    private const TARGET_CTYPE = 'tt3facts_tt3_facts';

    public function getTitle(): string
    {
        return '[teufels] Facts/Reasons: Migrate plugin';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all existing plugin to new one. Count of plugins: ' . count($this->getMigrationRecords());
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return count($this->getMigrationRecords()) > 0;
    }

    public function performMigration(): bool
    {
        $records = $this->getMigrationRecords();

        foreach ($records as $record) {
            $this->updateContentElement($record['uid'], self::TARGET_CTYPE);
        }

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid', 'CType')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq(
                    'CType',
                    $queryBuilder->createNamedParameter(self::SOURCE_CTYPE)
                )
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }


    /**
     * Updates Ctype, backendtitle and plugindata of the given content element UID
     *
     * @param int $uid
     * @param string $newCtype
     */
    protected function updateContentElement(int $uid, string $newCtype): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
        $queryBuilder->update('tt_content')
            ->set('CType', $newCtype)
            ->set('backendtitle',$queryBuilder->quoteIdentifier('tx_hivefacts_backendtitle'),false)
            ->set('tx_tt3facts_duration',$queryBuilder->quoteIdentifier('tx_hivefacts_duration'),false)
            ->set('tx_tt3facts_element',$queryBuilder->quoteIdentifier('tx_hivefacts_element'),false)
            ->where(
                $queryBuilder->expr()->in(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, Connection::PARAM_INT)
                )
            )
            ->executeStatement();
    }

}
