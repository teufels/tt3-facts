<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_facts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Facts\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3factsDataUpdater')]
class DataUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Facts/Reasons: Migrate data';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all data from tx_hivefacts_element to the new tx_tt3facts_element';
        if($this->checkMigrationTableExist()) { $description .= ': ' . count($this->getMigrationRecords()); }
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return $this->checkMigrationTableExist();
    }

    public function performMigration(): bool
    {
        //data
        $sql = "INSERT INTO tx_tt3facts_element(uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,element_backendtitle,element_decimalchar,element_decimalplaces,element_duration,element_grouping,element_image,element_prefix,element_seperator,element_startvalue,element_subtitle,element_suffix,element_text,element_title,element_value,record_type) 
        SELECT uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,tx_hivefacts_element_backendtitle,tx_hivefacts_element_decimal,tx_hivefacts_element_decimalplaces,tx_hivefacts_element_duration,tx_hivefacts_element_grouping,tx_hivefacts_element_image,tx_hivefacts_element_prefix,tx_hivefacts_element_seperator,tx_hivefacts_element_startvalue,tx_hivefacts_element_subtitle,tx_hivefacts_element_suffix,tx_hivefacts_element_text,tx_hivefacts_element_title,tx_hivefacts_element_value,record_type 
        FROM tx_hivefacts_element WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3facts_element');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();
        
        //sys_file_reference
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('sys_file_reference');
        $queryBuilder
            ->update('sys_file_reference')
            ->set('tablenames', 'tx_tt3facts_element')
            ->set('fieldname', 'element_image')
            ->where(
                $queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_hivefacts_element'))
            )
            ->executeStatement();

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_hivefacts_element');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid')
            ->from('tx_hivefacts_element')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function checkMigrationTableExist(): bool {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_hivefacts_element')
            ->getSchemaManager()
            ->tablesExist(['tx_hivefacts_element']);
    }
}
