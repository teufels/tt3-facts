<?php
defined('TYPO3') or die();

call_user_func(function () {
    
    /**
     * Register icons
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tt3facts_plugin_icon',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:tt3_facts/Resources/Public/Icons/Plugin.svg',
        ]
    );

});

