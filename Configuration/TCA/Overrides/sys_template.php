<?php
defined('TYPO3') or die();

$extensionKey = 'tt3_facts';
$extensionTitle = '[teufels] Facts/Reasons';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);