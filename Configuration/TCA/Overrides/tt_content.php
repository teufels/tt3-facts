<?php
defined('TYPO3') or die();

call_user_func(function () {

    //Adding Custom CType Item Group
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
        'tt_content',
        'CType',
        'teufels',
        'teufels',
        'after:special'
    );

    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['tt3facts_tt3_facts'] = 'tt3facts_plugin_icon';
    $tempColumns = [
        'backendtitle' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
                'type' => 'input',
            ],
            'description' => 'Backend Title (not visible in frontend)',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_facts/Resources/Private/Language/locallang_db.xlf:tt_content.backendtitle',
        ],
        'tx_tt3facts_duration' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'default' => '2',
                'eval' => 'int',
                'type' => 'input',
            ],
            'description' => 'global animation duration in seconds (if not set in Element)',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3facts_duration',
        ],
        'tx_tt3facts_element' => [
            'config' => [
                'appearance' => [
                    'collapseAll' => true,
                    'enabledControls' => [
                        'dragdrop' => true,
                    ],
                    'expandSingle' => true,
                    'levelLinksPosition' => 'both',
                    'showAllLocalizationLink' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => true,
                    'showSynchronizationLink' => true,
                    'useSortable' => true,
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'foreign_field' => 'parentid',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_tt3facts_element',
                'foreign_table_field' => 'parenttable',
                'type' => 'inline',
                'minitems' => 1,
                'maxitems' => 99,
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3facts_element',
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        'LLL:EXT:tt3_facts/Resources/Private/Language/locallang_db.xlf:tt_content.CType.tt3facts_tt3_facts',
        'tt3facts_tt3_facts',
        // Icon
        'tt3facts_plugin_icon',
        // The group ID, if not given, falls back to "none" or the last used --div-- in the item array
        'teufels'
    ];
    $tempTypes = [
        'tt3facts_tt3_facts' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,backendtitle,tx_tt3facts_duration,tx_tt3facts_element,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames, --palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
        ],
    ];
    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

    /**
     * Preview Renderer (needed for TYPO3 v12 backwards compatibility)
     */
    $GLOBALS['TCA']['tt_content']['types']['tt3facts_tt3_facts']['previewRenderer'] = \Teufels\Tt3Facts\Preview\PreviewRenderer::class;
});

