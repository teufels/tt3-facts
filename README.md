[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--facts-orange.svg)](https://bitbucket.org/teufels/tt3-facts/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__facts-red.svg)](https://bitbucket.org/teufels/tt3-facts/src/main/)
![version](https://img.shields.io/badge/version-1.1.*-yellow.svg?style=flat-square)

[ ṯeufels ] Facts/Reasons
==========
Provide Facts & Reasons Content-Element based on CountUp.js

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-facts`

***

### CountUp.js Documents
* [Starting Guide](https://inorganik.github.io/countUp.js/)
* [GitHub](https://github.com/inorganik/CountUp.js)

***

### Requirements
`CountUp.js: >=2.8.0 (UMD)`

***

### How to use
- Install with composer
- Import Static Template (before sitepackage)
- make own Layout by override Partials (and if needed Theme) in sitepackage

***

### Update & Migration from hive_facts
1. in composer.json replace `beewilly/hive_facts` with `"teufels/tt3-facts":"^1.0"`
2. Composer update
3. Include TypoScript set `[teufels] Facts/Reasons` 
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_facts yet)
5. Perform Upgrade Wizards `[teufels] Facts/Reasons`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_facts now)
7. class & id changed -> adjust styling in sitepackage (e.g. hive-facts-item => facts-item)
8. check & adjust be user group access rights

***

### Changelog
#### 1.1.x
- add support for TYPO3 v13
  - custom preview renderer only used for TYPO3 v12, TYPO3 v13 uses default renderer
  - add Previews for TYPO3 v13 and TYPO3 v12 support
  - update CountUp.js (UMD) to 2.8.0
#### 1.0.x
- 1.0.5 refactored CountUp initialization logic into a reusable function 
- 1.0.4 fix renderContentElementPreviewFromFluidTemplate changed parameters_
- 1.0.3 add Icon to ext_emconf
- 1.0.2 change c-<id> to c<id> & improve BE Preview
- 1.0.1 changed deprecated allowTableOnStandardPages() to ignorePageTypeRestriction (https://docs.typo3.org/m/typo3/reference-tca/12.4/en-us/Ctrl/Properties/Security.html#ctrl-security-ignorepagetyperestriction)
- 1.0.0 intial from [hive_facts](https://bitbucket.org/teufels/hive_facts/src/)