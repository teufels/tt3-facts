CREATE TABLE tt_content (
    backendtitle varchar(255) DEFAULT '' NOT NULL,
    tx_tt3facts_duration tinytext,
    tx_tt3facts_element int(11) unsigned DEFAULT '0' NOT NULL
);
CREATE TABLE tx_tt3facts_element (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    element_backendtitle tinytext,
    element_decimalchar tinytext,
    element_decimalplaces tinytext,
    element_duration tinytext,
    element_grouping int(11) DEFAULT '0' NOT NULL,
    element_image int(11) unsigned DEFAULT '0' NOT NULL,
    element_prefix tinytext,
    element_seperator tinytext,
    element_startvalue tinytext,
    element_subtitle tinytext,
    element_suffix tinytext,
    element_text mediumtext,
    element_title tinytext,
    element_value tinytext,
    record_type varchar(100) NOT NULL DEFAULT '0',
    KEY language (l10n_parent,sys_language_uid)
);
