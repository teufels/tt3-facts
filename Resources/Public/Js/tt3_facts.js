$(document).ready(function () {
    gsap.registerPlugin(ScrollTrigger);

    // Function to initialize the CountUp effect for a specific item
    function initializeCountUp($item) {
        // Get the value and duration data attributes
        var $value = parseFloat($item.find(".value").data("value"));
        var $duration = parseInt($item.find(".value").data("duration"));
        if ($duration === 0) {
            $duration = parseInt($item.find(".value").data("duration-global"));
        }

        // Get the unique identifier for the fact item
        var $factIdentifier = $item.find(".value").attr("id");

        // Set up CountUp.js options
        const options = {
            startVal: parseFloat($item.find(".value").data("start-value")),
            decimalPlaces: parseInt($item.find(".value").data("decimalplaces")),
            duration: $duration,
            useGrouping: Boolean($item.find(".value").data("usegrouping")),
            separator: $item.find(".value").data("separator"),
            decimal: $item.find(".value").data("decimal"),
            prefix: $item.find(".value").data("prefix"),
            suffix: $item.find(".value").data("suffix"),
        };

        // Create a new CountUp instance
        var countUpElement = new countUp.CountUp($factIdentifier, $value, options);

        // Trigger the CountUp animation when the element comes into view
        gsap.to($item.find(".value"), {
            scrollTrigger: {
                trigger: $item,
                start: "top bottom", // Animation starts when the item enters the viewport
                end: "bottom top", // Animation ends when the item leaves the viewport
                once: true, // Animation runs only once
                onEnter: () => {
                    if (!countUpElement.error) {
                        countUpElement.start();
                    } else {
                        console.error(countUpElement.error);
                    }
                },
            },
        });
    }

    // Initialize all existing fact items on page load
    $(".tx_tt3_facts .facts-item.fact").each(function () {
        initializeCountUp($(this));
    });

    // Make the function available globally to initialize dynamic items later
    window.initializeCountUpForItem = function ($item) {
        initializeCountUp($item);
    };
});
